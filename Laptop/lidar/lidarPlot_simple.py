import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
import math


def update_plot(k, scat, degsPerP):
    #scat.set_array(data[i])
    #print 'K: ', k
    myDistances = [20,21,2,3,4,5,6,7,8,9,2, 2,3]
    xData = [0 for i in range(len(myDistances))]
    yData = [0 for i in range(len(myDistances))]
    # generate x and y coordinates from distances
    for i in range(0,len(myDistances)):
        xData[i] = myDistances[i] * math.cos(math.radians(180+45-i*degsPerP))
        yData[i] = myDistances[i] * math.sin(math.radians(180+45-i*degsPerP))
    print np.swapaxes(np.asarray((xData,yData)),0,1).shape
    scat.set_offsets(np.swapaxes(np.asarray((xData,yData)),0,1))

    return scat


def main():
    myDistances = [10,11,12,13,14,15,16,17,18,19,20, 22, 23]

    degsPerP = 270.0/(len(myDistances)-1)
    print 'degs per P: ', degsPerP

    scannerPosx = 0
    scannerPosy = 0

    xData = [0 for i in range(len(myDistances))]
    yData = [0 for i in range(len(myDistances))]
    # generate x and y coordinates from distances
    for i in range(0,len(myDistances)):
        xData[i] = myDistances[i] * math.cos(math.radians(-45+i*degsPerP))
        yData[i] = myDistances[i] * math.sin(math.radians(-45+i*degsPerP))
        #print 'i:',i, 'd:',myDistances[i], ', x:', xData[i], ', y:', yData[i]

    xData = [0]+xData # add cross in origin
    yData = [0]+yData
    fig = plt.figure()
    scat = plt.scatter(xData,yData, s=20, marker='x')
    plt.xlim(-25, 25)
    plt.ylim(-25, 25)
    #print len(yData)

    ani = animation.FuncAnimation(fig, update_plot, frames=xrange(5), fargs=(scat, degsPerP))
    plt.show()

main()

'''
xDatap1 = [1,2,3,4,5]
yDatap1 = [0.1,0.1,0.1,0.1,0.1]

xDatap2 = [9,9,9,9,9]
yDatap2 = [9,8,7,6,5]

xDatap3 = [0.1,0.2,0.3,0.4,0.5]
yDatap3 = [0.9,0.7,0.6,0.4,0.5]

pos_data = np.random.random((5, 3, 2))

for i in range(0, 5):
    #pos_data[i] = [[xDatap1[i], xDatap2[i], xDatap3[i]], [yDatap1[i],yDatap2[i],yDatap3[i]]]
    pos_data[i] = [[xDatap1[i],yDatap1[i]],[xDatap2[i],yDatap2[i]],[xDatap3[i],yDatap3[i]]]

numframes = 5
numpoints = 3
#pos_data = np.random.random((numframes, numpoints, 2))
#pos_data = (numframes,2,2)
x, y, c = np.random.random((3, numpoints))

fig = plt.figure()
scat = plt.scatter([xDatap1[0],xDatap2[0], xDatap3[0]],
                    [yDatap1[0],yDatap2[0], yDatap3[0]], c=c, s=100, marker='x')
plt.xlim(0, 15)
plt.ylim(0, 10)

ani = animation.FuncAnimation(fig, update_plot, frames=xrange(5),
                              fargs=(pos_data, scat))
plt.show()
'''