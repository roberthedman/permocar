import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
import math
import socket
import time

LIDAR_IP = '10.180.65.30' #'169.254.13.125'
PLOTTER_IP = '10.9.0.2' #'169.254.13.125'
LIDAR_PORT = 2111
PLOTTER_PORT = 2110
BUFFER_SIZE = 10240
MESSAGE = "\x02sRN LMDscandata\x03"




def main():


    plot_type = "blit"

    sp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sp.bind(('', PLOTTER_PORT))
    print 'listening for sender...'
    sp.listen(1)
    conn, addr = sp.accept()
    print 'Connected by', addr
    #conn.send("Hello")
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((LIDAR_IP, LIDAR_PORT))

    while True:
    	# check if update needed
    	state = conn.recv(2)
    	print(state)
    	if state == "go":
    		print("state was go")
    		# request lidar data
    		s.sendall(MESSAGE)
    		data = s.recv(BUFFER_SIZE)
    		print data
    		#print data

    		#time.sleep(0.1)

    		# send lidar data
    		conn.sendall(data)
    
    s.close()
    conn.close()

main()
