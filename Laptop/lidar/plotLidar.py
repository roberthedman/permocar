import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
import math
import socket
import time

TCP_IP = '10.180.65.30' #'169.254.13.125'
TCP_PORT = 2111
BUFFER_SIZE = 10240
MESSAGE = "\x02sRN LMDscandata\x03"

xData = [0 for i in range(0,541)]
yData = [0 for i in range(0,541)]
degsPerP = 270.0/(541-1)

def update_plot(k, scat, s):

    s.send(MESSAGE)
    data = s.recv(BUFFER_SIZE)
    try:  
        decdata = map(lambda x: int(x,16), data.split(' 21D ')[1].split(' ')[0:-1])[0:-5]
        if len(decdata) != 541 :      
            print 'len: ', len(decdata)
            degsPerP = 270.0/(len(myDistances)-1)
            # generate x and y coordinates from distances
            for i in range(0,len(myDistances)):
                xData[i] = myDistances[i] * math.cos(math.radians(-45+i*degsPerP))
                yData[i] = myDistances[i] * math.sin(math.radians(-45+i*degsPerP))
            #print np.swapaxes(np.asarray((xData,yData)),0,1).shape
            scat.set_offsets(np.swapaxes(np.asarray((xData,yData)),1,0))

            return scat

        else:
            #print 'OK!'
            myDistances = decdata
            degsPerP = 0.5 #

            # generate x and y coordinates from distances
            for i in range(0,len(myDistances)):
                xData[i] = myDistances[i] * math.cos(math.radians(-45+i*degsPerP))
                yData[i] = myDistances[i] * math.sin(math.radians(-45+i*degsPerP))
            #print np.swapaxes(np.asarray((xData,yData)),0,1).shape
            scat.set_offsets(np.swapaxes(np.asarray((xData,yData)),1,0))

            return scat
    except :
        print ('some error\n')
        return scat




def main():


    plot_type = "blit"
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, TCP_PORT))

    myDistances = [10,11,12,13,14,15,16,17,18,19,20, 22, 23]

    #degsPerP = 270.0/(len(myDistances)-1)
    print 'degs per P: ', degsPerP

    scannerPosx = 0
    scannerPosy = 0

    xData = [0 for i in range(len(myDistances))]
    yData = [0 for i in range(len(myDistances))]
    # generate x and y coordinates from distances
    for i in range(0,len(myDistances)):
        xData[i] = myDistances[i] * math.cos(math.radians(180+45-i*degsPerP))
        yData[i] = myDistances[i] * math.sin(math.radians(180+45-i*degsPerP))
        #print 'i:',i, 'd:',myDistances[i], ', x:', xData[i], ', y:', yData[i]

    xData = [0]+xData # add cross in origin
    yData = [0]+yData
    fig = plt.figure()
    scat = plt.scatter(xData,yData, s=0.5, marker='x')
    plt.xlim(-20000, 20000)
    plt.ylim(-10000, 20000)
    fig.gca().set_aspect('equal', adjustable='box')
    #print len(yData)

    ani = animation.FuncAnimation(fig, update_plot, interval=20, frames=xrange(2), fargs=(scat, s))
    plt.show()
    s.close()

main()



#import matplotlib.pyplot as plt



#start
#MES
