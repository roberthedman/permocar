import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
import math
import socket
import time

TCP_IP = '10.9.0.3' #'169.254.13.125'
TCP_PORT = 2110
BUFFER_SIZE = 10240

xData = [0 for i in range(0,541+5)]
yData = [0 for i in range(0,541+5)]
degsPerP = 270.0/(541-1)

def update_plot(k, scat, s):


    s.send("go");
    #time.sleep(0.1)
    data = s.recv(BUFFER_SIZE)
    try:  
        decdata = map(lambda x: int(x,16), data.split(' 21D ')[1].split(' ')[0:-1])[0:-5]
        if len(decdata) != 541 :      
            print 'len: ', len(decdata)
            degsPerP = 270.0/(len(myDistances)-1)
            # generate x and y coordinates from distances
            for i in range(0,len(myDistances)):
                xData[i] = myDistances[i] * math.cos(math.radians(-45+i*degsPerP))
                yData[i] = myDistances[i] * math.sin(math.radians(-45+i*degsPerP))
            #print np.swapaxes(np.asarray((xData,yData)),0,1).shape
            
            #xData = [0]+xData # add cross in origin
            #yData = [0]+yData

            scat.set_offsets(np.swapaxes(np.asarray((xData,yData)),1,0))

            return scat

        else:
            print 'OK! 1'
            myDistances = decdata
            print 'OK! 2'
            print len(myDistances)
            degsPerP = 0.5 #

            # generate x and y coordinates from distances
            for i in range(0,len(myDistances)):
                #print i
                xData[i] = myDistances[i] * math.cos(math.radians(-45+i*degsPerP))
                yData[i] = myDistances[i] * math.sin(math.radians(-45+i*degsPerP))

            xData[len(myDistances)] = 0
            yData[len(myDistances)] = 0
            yData[len(myDistances)+1] = 200
            xData[len(myDistances)+1] = 300

            yData[len(myDistances)+2] = 200
            xData[len(myDistances)+2] = -300
            
            yData[len(myDistances)+3] = -650
            xData[len(myDistances)+3] = 300
            
            yData[len(myDistances)+4] = -650
            xData[len(myDistances)+4] = -300
            #print np.swapaxes(np.asarray((xData,yData)),0,1).shape
            print 'OK! 3'
            #print xData[0]
            #print 'OK! 4'
            #print xData
            #print 'OK! 5'
            #xData = [0]+xData # add cross in origin
            #yData = [0]+yData

            #print xData

            scat.set_offsets(np.swapaxes(np.asarray((xData,yData)),1,0))

            return scat
    except :
        print ('some error\n')
        return scat




def main():


    plot_type = "blit"
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, TCP_PORT))

    myDistances = [10,11,12,13,14,15,16,17,18,19,20, 22, 23]

    #degsPerP = 270.0/(len(myDistances)-1)
    print 'degs per P: ', degsPerP

    scannerPosx = 0
    scannerPosy = 0

    xData = [0 for i in range(len(myDistances))]
    yData = [0 for i in range(len(myDistances))]
    # generate x and y coordinates from distances
    for i in range(0,len(myDistances)):
        xData[i] = myDistances[i] * math.cos(math.radians(180+45-i*degsPerP))
        yData[i] = myDistances[i] * math.sin(math.radians(180+45-i*degsPerP))
        #print 'i:',i, 'd:',myDistances[i], ', x:', xData[i], ', y:', yData[i]

    xData = [0]+xData # add cross in origin
    yData = [0]+yData
    fig = plt.figure()
    scat = plt.scatter(xData,yData, s=0.5, marker='x')
    plt.xlim(-5000, 5000)
    plt.ylim(-3000, 7000)
    fig.gca().set_aspect('equal', adjustable='box')
    #print len(yData)

    print "Starting animation"
    ani = animation.FuncAnimation(fig, update_plot, interval=500, frames=xrange(2), fargs=(scat, s))
    plt.show()
    s.close()

main()



#import matplotlib.pyplot as plt



#start
#MES
