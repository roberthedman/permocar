#!/bin/bash


echo "starting controller"
( python Robot/python/permobil/sender_udp_v2.py &> /dev/null ) &
P0=$!

echo "starting camera receivers"

( ./scream/scream_receiver/bin/scream_receiver 10.9.0.3 31110 |& tee reciever.log ) &
P1=$!


( gst-launch-1.0 udpsrc port=30130 ! "application/x-rtp,payload=98,encoding-name=H264" ! rtpjitterbuffer latency=10 ! rtph264depay ! avdec_h264 ! videoconvert ! xvimagesink sync=false ) &
P2=$!

( gst-launch-1.0 udpsrc port=30132 ! "application/x-rtp,payload=98,encoding-name=H264" ! rtpjitterbuffer latency=10 ! rtph264depay ! avdec_h264 ! videoconvert ! xvimagesink sync=false ) &
P3=$!

( gst-launch-1.0 udpsrc port=30134 ! "application/x-rtp,payload=98,encoding-name=H264" ! rtpjitterbuffer latency=10 ! rtph264depay ! avdec_h264 ! videoconvert ! xvimagesink sync=false ) &
P4=$!

( gst-launch-1.0 udpsrc port=30136 ! "application/x-rtp,payload=98,encoding-name=H264" ! rtpjitterbuffer latency=10 ! rtph264depay ! avdec_h264 ! videoconvert ! xvimagesink sync=false ) &
P5=$!


echo "Receiver and camera pipes up!"
wait $P0 $P1 $P2 $P3 $P4 $P5

