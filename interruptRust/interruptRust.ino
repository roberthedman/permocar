
const byte interruptPinL = 3;
const byte interruptPinR = 8;
volatile long countL = 0;
volatile long countR = 0;
byte *countPtrL = (byte*) &countL;
byte *countPtrR = (byte*) &countR;

const long timeThresh = 500; //ms
long lastTime = 0;
volatile long lastCountL = 0;
volatile long lastCountR = 0;
const long countTimeThresh = 100;

void setup() {
  pinMode(interruptPinL, INPUT_PULLUP);
  pinMode(interruptPinR, INPUT_PULLUP);
  Serial.begin(9600);
  lastTime = millis();
  attachInterrupt(digitalPinToInterrupt(interruptPinL), myInterruptL, RISING);
  attachInterrupt(digitalPinToInterrupt(interruptPinR), myInterruptR, RISING);
}

void loop() {
  if(millis() - lastTime > timeThresh) {
    
    lastTime = millis();

    for(byte i=3;i<4;i--) { // underflows
      Serial.write(*(countPtrL+i));
    }
    for(byte i=3;i<4;i--) { // underflows
      Serial.write(*(countPtrR+i));
    }

    countL=0;
    countR=0;
  }
}

void myInterruptL() {
  if(millis() - lastCountL > countTimeThresh) {
    countL+=50;
    lastCountL = millis();
  }
}
void myInterruptR() {
  if(millis() - lastCountR > countTimeThresh) {
    countR+=50;
    lastCountR = millis();
  }
}
