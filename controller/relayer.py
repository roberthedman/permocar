#!/usr/bin/python

import serial
import struct
import pygame
import math
import socket

#################################################
#   BEGIN SETTINGS
#################################################

# Serial
#PORT = '/dev/ttyUSB0'
PORT = '/dev/ttyACM0'
BAUDRATE = 57600
TIMEOUT = 0.2
LIVE = True

# Initialize Serial
if LIVE:
	ser = serial.Serial()
	ser.port = PORT
	ser.baudrate = BAUDRATE
	ser.timeout = TIMEOUT
	ser.write_timeout = 1

	ser.open()

HOST = ''                 # Symbolic name meaning all available interfaces
PORT = 50007              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
print 'listening for sender...'
s.listen(1)
conn, addr = s.accept()
print 'Connected by', addr
while 1:
    data = conn.recv(4096)
    print data
    #data = map(int, data.split('\r\n')[0].split(',')) #.rstrip('\r\n')
    data = data.rstrip('\r\n').split('\r\n')	#split each send as item in list
    if (len(data) > 1 ):
	print('I am not fast enough')
	print data
	print len(data)
	data = 0
	continue
    data = data[0]
    data = map(int, data.split(','))
    print data
    if (data[0] == 98 and data[11] == 101):
	ser.reset_output_buffer()
	ser.reset_input_buffer()
    	ser.write(data)
    if not data: break
    data = 0

    #conn.sendall(data)
conn.close()



# Main program loop
run = True
while run:

	# Set mData based on jData
	processInput(jData, jData2, mData, macros)

	# Make sure data is bounded
	sanityCheck(mData)

	# Write data to serial
	if LIVE:
	    writeSerialData(ser, mData)

	    # print("Reading")
	    if ser.readline():
		print("OK")
	    else:
		print("DATA LOSS")
		mData.dataLoss += 1

	drawText(s, jData, jData2, mData, macros)
	drawMeters(s, jData, jData2, mData)

	pygame.display.flip()
	clock.tick(FRAMERATE)

if LIVE:
	if(ser.isOpen()):
	    ser.close()

if __name__ == "__main__":
    main()
