# Permocar 
The code in this repo are all from the original permocar project at university.
It was a ROS based permobil/wheelchair chassis with custom motor drivers, an arduino for all low level control, a custom serial interface between the arduino and a raspberry pi which handled all heavy lifting, such as SLAM with the encoders, a lidar, etc.
