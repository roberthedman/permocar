use std::thread;
use std::sync::mpsc;
use std::time;

// stuff for tcp 
use std::io::prelude::*;
use std::net::TcpStream;
use std::net::TcpListener;

// stuff for serial
extern crate serial;
use std::io;
use std::time::Duration;
//use std::io::prelude::*;
use serial::prelude::*;

//use std::str;


/////////////////////////////
///////   constants   ///////
/////////////////////////////

//lidar
const LIDAR_ADDRESS: &str = "127.0.0.1:8080";
const LIDAR_BUF_SIZE: usize = 4096;

//Arduinos
const COUNTER_SERIAL_PORT: &str = "/dev/ttyACM1";
const DRIVER_SERIAL_PORT: &str = "/dev/ttyACM0";

//controller
const REMOTE_OPERATOR_ADDRESS: &str = "10.9.0.3:50007"; // remember to double check port!
const CONTROLLER_SAMPLING_TIME: u32 = 200; //ms

// each pixel represents 1dm*1dm, so 100*100mm.
const ROWS: usize = 1000;
const COLS: usize = 1000;

// robot real stuff
const AXEL_WIDTH: f32 = 175.0; // cm
const FREQ_TO_OMEGA: f32 = 0.0002;
const WHEEL_RADIUS: f32 =  30.0; //cm

/////////////////////////////
///////    structs    ///////
/////////////////////////////
struct RobotState {
    x: f32,
    y: f32,
    theta: f32,
    xdot: f32,
    ydot: f32,
}

//#![feature(box_syntax)]
fn main() {

    let mut map = Box::new([[0u32; ROWS]; COLS]);

    // communicators between threads
    let (mapper_request_location_tx, request_location_rx): (mpsc::Sender<u8>, mpsc::Receiver<u8>) = mpsc::channel();
    let controller_request_location_tx = mapper_request_location_tx.clone();
    
    let (controller_send_location_tx, controller_send_location_rx): (mpsc::Sender<(f32, f32, f32, f32, f32)>, mpsc::Receiver<(f32, f32, f32, f32, f32)>) = mpsc::channel();
    let (mapper_send_location_tx, mapper_send_location_rx): (mpsc::Sender<(f32, f32, f32, f32, f32)>, mpsc::Receiver<(f32, f32, f32, f32, f32)>) = mpsc::channel();


    let builder = thread::Builder::new().name("controller".into());
    println!("starting controller...");
    let controller = builder.spawn(move || {
        println!("in controller....");
        let mut stream;
        let mut state: (f32, f32, f32, f32, f32) = (COLS as f32/2.0,ROWS as f32/2.0, 0.0, 0.0, 0.0); 
        
        let mut port: serial::SystemPort;
        loop{
            match  serial::open(&DRIVER_SERIAL_PORT) {
                Ok(p) => {port = p; break;},
                Err(e) => {
                    println!("Controller: Could not connect to arduino driver: {}", e); 
                    thread::sleep(time::Duration::new(1,0));
                    },
            }
        }
        interact(&mut port).unwrap();

        let listener: std::net::TcpListener;
        println!("controller: waiting for sender");
        loop {
            match TcpListener::bind(REMOTE_OPERATOR_ADDRESS) {
                Ok(lis) => {listener = lis; break;},
                Err(e) => {
                    println!("Controller: No connection from operator yet. {}", e);
                    thread::sleep(time::Duration::new(1,0));
                    }, 
            }
        }

        match listener.accept() {
            Ok((socket, addr)) => {
                println!("controller: new client: {:?}", addr); stream=socket;

                // the message format is defined as: (as incoming bytes)
                // [SOM, L_dir, L_speed, R_dir, R_speed, EOM]
                // where: L_dir = 0 meanst stop, 1 forward and 2 rev, same for R
                //          speed is a byte from 0-200 determining throttle position (201 - 255 are reserved.)
                // special bytes:
                // SOM (start of message): 250, EOM (end of message): 251
                const message_length: usize = 6;

                let mut message: [u8; message_length] = [0; message_length];
                let mut received_bytes = 0;
                let mut recording = false;
                let mut sucessful_receive = false;

                loop {
                    // get controller input (We expect to set soloMode here.)
                    for r in stream.try_clone().expect("stream clone failed").bytes() {
                        match r {
                            Ok(b) => {
                                if b == 250 && !recording {
                                    //println!("Server found start of message");
                                    message[received_bytes] = b;
                                    recording = true;
                                    received_bytes+=1;
                                }
                                else if b == 251 {
                                    message[received_bytes] = b;
                                    received_bytes+=1; // since length now is total bytes, not index
                                    //println!("Server found end of message");
                                    print!("Controller got: ");
                                    for k in 0..received_bytes {
                                        print!("{}, ", message[k] );
                                    }
                                    print!("\n");
                                    
                                    //let written_size = stream.write(&[69 as u8, 96 as u8]);
                                    //let written_size = stream.write_all(b"\x02This is a bunch of lidar data...\x03");
                                    //println!("server wrote to stream.");

                                    // reset everything for next message.
                                    recording = false;
                                    received_bytes = 0;
                                    sucessful_receive = true;
                                    break;
                                }
                                else if recording {
                                    message[received_bytes] = b;
                                    received_bytes+=1;
                                }

                            },
                            Err(_) => {}, //TODO
                        }
                        if received_bytes > message_length && recording { // EOM is missing!
                            received_bytes = 0;
                            recording = false;
                        }
                    }


                    // get motors current speeds
                    //println!("Controller: requesting pos data.");
                    //controller_request_location_tx.send(10 as u8).unwrap();
                    //state = controller_send_location_rx.recv().unwrap();

                    // PID controller

                    // send voltage to motors.
                    if sucessful_receive {
                        sucessful_receive = false;
                        match port.write(&message[0..message_length]) {
                            Ok(_) => { 
                                //println!("Controller: sent {} bytes to arduino!", n);
                                },
                            Err(e) => {println!("Controller: could not write to arduino: {}", e); },
                        }
                    }

                }
            
                
            },
            Err(e) => println!("couldn't get client: {:?}", e), // TODO: what if controller braks? Thread ends?
        }
    }).unwrap();

    //let mut vecMap = vec![vec![0; ROWS]; COLS];


    //let mut map = [[0u32; ROWS]; COLS];
    //map[0][0] = 3;

    //println!("{}, {}", map[0][0], map[0][1]);

    // dummy thread for replacing lidar
    let builder = thread::Builder::new().name("DummyLidarThread".into());
    let server = builder.spawn(|| {
        let listener = TcpListener::bind(LIDAR_ADDRESS).unwrap();
        let mut stream;
        match listener.accept() {
            Ok((socket, addr)) => {
                println!("server: new client: {:?}", addr); stream=socket;
                thread::sleep(time::Duration::new(1,0));

                let mut message: [u8; 2048] = [0; 2048];
                let mut message_len = 0;
                let mut recording = false;
                

                for r in stream.try_clone().expect("stream clone failed").bytes() {
                    match r {
                        Ok(b) => {
                            if b == 2 && !recording {
                                //println!("Server found start of message");
                                message[message_len] = b;
                                recording = true;
                                message_len+=1;
                            }
                            else if b == 3 {
                                message[message_len] = b;
                                message_len+=1; // since length now is total bytes, not index
                                //println!("Server found end of message");
                                println!("Server got: {:?}", String::from_utf8_lossy(&message[0..message_len]));
                                //let written_size = stream.write(&[69 as u8, 96 as u8]);
                                let written_size = stream.write_all(b"\x02This is a bunch of lidar data...\x03");
                                println!("server wrote to stream.");

                                // reset everything for next message.
                                recording = false;
                                message_len = 0;
                                //break;
                            }
                            else if recording {
                                message[message_len] = b;
                                message_len+=1;
                            }

                        },
                        Err(_) => {},
                    } 
                }
                
            },
            Err(e) => println!("couldn't get client: {:?}", e),
        }
    }).unwrap();


    // location tracker
    let builder = thread::Builder::new().name("Location Tracker Thread".into());
    let tracker = builder.spawn(move || {
        println!("Tracker started.");
        
        let mut serial_buf: [u8; 8] = [0; 8];
        let mut port: serial::SystemPort;
        loop{
            match  serial::open(&COUNTER_SERIAL_PORT) {
                Ok(p) => {port = p; break;},
                Err(e) => {
                    println!("LocationTracker: Could not connect to arduino counter: {}", e); 
                    thread::sleep(time::Duration::new(1,0));
                    },
            }
        }
        interact(&mut port).unwrap();

        //let mut l_tick: f32 = 0;
        //let mtu r_tick: f32 = 0;
        // state: x, y, theta, xdot, ydot
        let mut state: (f32, f32, f32, f32, f32) = (COLS as f32/2.0,ROWS as f32/2.0, 0.0, 0.0, 0.0);
        println!("location initial position: {:?}", state);
        loop {
            // get encoder data from arduino and update local variables
            match port.read_exact(&mut serial_buf){
                Ok(_) => {
                    let l_tick: f32 = tick_to_speed( ( ((serial_buf[0] as u32) << 24) + ((serial_buf[1] as u32) << 16) + ((serial_buf[2] as u32) << 8) + (serial_buf[3] as u32) ) as i32);
                    let r_tick: f32 = tick_to_speed( ( ((serial_buf[4] as u32) << 24) + ((serial_buf[5] as u32) << 16) + ((serial_buf[6] as u32) << 8) + (serial_buf[7] as u32) ) as i32);
                    update_state(&mut state, l_tick, r_tick);
                    print!("L: {}, ", state.0);
                    println!("R: {}", state.1);
                    },
                Err(e) => {
                    println!("Tracker: Could not read all bytes: {:?}", e);
                },
            }


            // check if position is requested
            // a byte will be sent, codes are:
            // 10: mapper, 11: controller
            match request_location_rx.try_recv() {
                Ok(msg) => {
                    match msg {
                        0 => {
                            println!("tracker got stop signal.");
                            break;
                        },
                        10 =>{
                            mapper_send_location_tx.send((state)).unwrap();
                        },
                        11 => {
                            controller_send_location_tx.send((state)).unwrap();
                        },
                        _ => {}, // nothing to send.
                    }

                },
                Err(_) => {
                    //println!("Error: {}, on recieving side.", e);
                    //thread::sleep_ms(100);
                    //pos.1 += 1;
                    },
            };
        };
        println!("tracker exiting with local pos: {:?}", state);
   
    }).unwrap();



    // mapper (gets lidar data, requests position and updates its map.)
    let builder = thread::Builder::new().name("Mapper Thread".into());
    let mapper = builder.spawn(move || {
        println!("mapper started.");
        // create empty map
        
        let mut pos: (f32, f32, f32, f32, f32); //= (0,0);

        // set up connection to lidar
        let mut stream = TcpStream::connect(&LIDAR_ADDRESS).unwrap();
        let mut lidar_buf: [u8; LIDAR_BUF_SIZE] = [0u8; LIDAR_BUF_SIZE];
        let mut message_len: usize = 0;
        let mut recording = false;

        // main loop for this thread
        loop {
            // request position data
            println!("Mapper requesting pos data.");
            mapper_request_location_tx.send(10 as u8).unwrap();
            pos = mapper_send_location_rx.recv().unwrap();
            
            println!("Mapper is at length: {}", message_len);

            // request lidar data
            //thread::sleep(time::Duration::new(3,0));    
            println!("mapper requesting lidar scan.");
            let _ = stream.write_all(b"\x02sRN LMDscandata\x03"); // tell lidar to send one measurment
            for r in stream.try_clone().expect("stream clone failed").bytes() { // iterate over answer
                match r {
                    Ok(b) => {
                        if b == 2 && !recording { // start of message
                            //println!("Server found start of message");
                            lidar_buf[message_len] = b;
                            recording = true;
                            message_len+=1;
                        }
                        else if b == 3 { // end of message
                            lidar_buf[message_len] = b;
                            message_len+=1; // since length now is total bytes, not index
                            //println!("Server found end of message");
                            println!("Mapper got (from lidar): {:?}", String::from_utf8_lossy(&lidar_buf[0..message_len]));
                            //let written_size = stream.write(&[69 as u8, 96 as u8]);
                            //println!("server wrote {:?} to stream.", written_size);

                            // update map using lidar data
                            update_map_with_lidar(lidar_buf, message_len, &mut map);

                            // reset everything for next message.
                            recording = false;
                            message_len = 0;
                            break;
                        }
                        else if recording {
                            lidar_buf[message_len] = b;
                            message_len+=1;
                        }

                    },
                    Err(_) => {},
                } 
            } 
            //let stream_write_response = stream.write(&[65 as u8, 66 as u8]);
            //println!("mapper wrote {:?} to stream.", stream_write_response);

            // update map

            // to shutdown tracker:
            // request_location_tx.send(false).unwrap();

            thread::sleep(time::Duration::new(2,0*50*1000000)); // from ms to ns
        };

        
   
    }).unwrap();


    println!("Main wating for join.");
    mapper.join().unwrap();
    tracker.join().unwrap();
    println!("Main wating for server join.");
    //drop(stream);
    server.join().unwrap();
    println!("end of main");

    controller.join().unwrap();

    // pathfinder, takes a map and returns a list of nodes to aim for when driving
    // This will be a function for mapper to call. 


}




fn interact<T: SerialPort>(port: &mut T) -> io::Result<()> {
    try!(port.reconfigure(&|settings| {
        try!(settings.set_baud_rate(serial::Baud57600));
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        Ok(())
    }));
 try!(port.set_timeout(Duration::from_millis(10000)));

    Ok(())
}

/*
const AXEL_WIDTH: f32 = 175; // cm
const FREQ_TO_OMEGA: f32 = 0.0002;
const WHEEL_RADIUS: f32 =  30.0; //cm
*/

fn tick_to_speed(tick: i32) -> f32 { 
    tick as f32 /10.0
}
fn update_state(pos: &mut (f32,f32,f32, f32, f32), l_speed: f32, r_speed: f32) {
    let diffx = 0.5*(l_speed + r_speed)*((pos.2).to_radians().sin());
    let diffy = 0.5*(l_speed + r_speed)*((pos.2).to_radians().cos());
    pos.3 = (diffx - pos.0) / (CONTROLLER_SAMPLING_TIME as f32 / 1000.0); // sampl time is in ms.
    pos.4 = (diffy - pos.1) / (CONTROLLER_SAMPLING_TIME as f32 / 1000.0);
    pos.0 += diffx;
    pos.1 += diffy;
    pos.2 += (r_speed - l_speed)/AXEL_WIDTH;

}
fn update_map_with_lidar(lidar_data: [u8; LIDAR_BUF_SIZE], message_len: usize, map: &mut [[u32; ROWS]; COLS]) {
    map[0][0] += 1;
}
